
import reactDom from 'react-dom';
import React ,{useEffect,useState}from 'react'
import AOS from "aos";
import { Route, Switch, Redirect ,BrowserRouter} from 'react-router-dom'
import Home from './Component/HomePage/Homepage'
import Assistants from './Component/Assistants/Assistants'
function App() {
  
React.useEffect(() => {
  AOS.init({ duration: 2000 });


  return () => {

  }
}, [])
  return (
  //   <React.Fragment>
  //   <div className="main-wrapper">
  //     <Switch>
    


  //     </Switch>
  //   </div>
  // </React.Fragment>
  <BrowserRouter>
  <Switch>
  <Route exact path='/assistants' component={Assistants} />
  <Route exact path='/' component={Home} />

  </Switch>
 </BrowserRouter>

  );
}

export default App;
