import React, { useState, useEffect, useRef } from "react";
import axios from 'axios'

import searchinput from '../../Image/searchinput.png'
import navdrop from '../../Image/navdrop.png'
import ReactDOM from "react-dom";
import './Assistants.scss'

export default function Assistants() {
    const Navbarlist=[
        {
            name:"All"
        },
    {
        name:"Email"
    },
    {
        name:"Name"
    }

]
    const [listdata,setlistdata]=useState([])
    const [searchlist,setsearchlist]=useState([])
    const [nav,setnav]=useState('All')
    const [inputnav,setinputnav]=useState("Search For Assistants")
    const [isComponentVisible, setIsComponentVisible] = useState(false);
      const ref = useRef(null);
    
      const handleHideDropdown = (event: KeyboardEvent) => {
        if (event.key === "Escape") {
          setIsComponentVisible(false);
        }
      };
    
      const handleClickOutside = async(event) => {
        if (ref.current && !ref.current.contains(event.target)) {
         await setIsComponentVisible(false);
         
        }
      };
    
      useEffect(() => {
        document.addEventListener("keydown", handleHideDropdown, true);
        document.addEventListener("click", handleClickOutside, true);
        return () => {
          document.removeEventListener("keydown", handleHideDropdown, true);
          document.removeEventListener("click", handleClickOutside, true);
        };
      });
    

    
 useEffect(() => {
    AssistantFucntion()
     return () => {
      
     }
 }, [])

 const AssistantFucntion = async ()=>{

    await axios
      .get(`https://comms.globalxchange.com/coin/vault/assistant/get`) 
      .then(res => {
        if (res.data.status) {
            setsearchlist(res.data.users)
            setlistdata(res.data.users)

   }

      });

  }
  const navinputfunction = async e => {
 let name="Shorupan Pirakaspathy"
    let value =e.target.value;
    let n1=""
if(nav==="All" ||nav==="UserName" )
{
    n1= searchlist.filter((user)=>{
        return (user.assistantTag).toUpperCase().includes((value).toUpperCase());
        
       })
}
else{
    n1= searchlist.filter((user)=>{
        return (user.email).toUpperCase().includes((value).toUpperCase());
        
       })  
}


  
  

   
    await setlistdata(n1)
  }

  const functionclosedrop=(item)=>{
    setnav(item)
    setIsComponentVisible(false)
  if(item==="All")
  {
    setinputnav(`Search For Assistants"`)
  }
  else{
    setinputnav(`Search By ${item}`)
  }
 
  
}
    return (
        <div className="AssistantDashboard" data-aos="fade-up">

<div className="AssistantSearch"  >
    <div className="insidesearchdsiplay" onClick={()=>setIsComponentVisible(true)}>
        <img src={navdrop} alt=""/>
    <p>{nav}</p>
    </div>
    <div  className="imputcontroller" >
    <input type="text" placeholder={`${inputnav}`} name="" id=""onChange={navinputfunction}/>
    <img src={searchinput} alt=""/>
    </div>

    {
        isComponentVisible?
    <div className="dropdownsearch" ref={ref} >
    {Navbarlist.map(item=>{
    return(
        <div className="insidedropdownsearch" onClick={()=>functionclosedrop(item.name)}>
            <img src={navdrop} alt=""/>
            <p>{item.name}</p>
        </div>
    )
})
          }
      </div>
        :
        null
    }

</div>

 
      
      
      <div>
          {
              listdata.map(item=>{
                  return(
                      <div className="Asssitantlist"> 
                      <div style={{display:"flex",alignItems:"center"}}>
                          <img src={item.profile_img} alt=""/>
                          <div style={{marginLeft:"1rem"}}>
                          <h3>{item.assistantTag}</h3>
                          <p>{item.email}</p>
                      </div>
                      </div>
                      <div style={{display:"flex"}}>
                          <label className="messagelabel" >Message</label>
                          <label className="meetinglabel" >Meeting</label>
                      </div>
                      </div>
                   
                  )
              })
          }
      </div>
    </div>
  )
}
